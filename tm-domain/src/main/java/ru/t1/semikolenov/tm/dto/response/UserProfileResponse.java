package ru.t1.semikolenov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.User;

@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable final User user) {
        super(user);
    }

}